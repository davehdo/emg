# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

# all normal

# not measured

# absent


$('select').bind 'change', (event) =>
	if $(event.target).children("option:selected").text() == "absent"
		val_of_absent_option = (j) -> 
			o = $(j)
			if o.text()=="absent"
				o.attr("value")
			else
				null
			
		absentize = (i) -> 
			val_of_absent = $.map($(i).children("option"), val_of_absent_option)
			if val_of_absent.length > 0
				$(i).val(val_of_absent[0]) 
			else
				$(i).val("0")

		$.map($(".nerve-segment-1 select"), absentize ) 
	
	# # submit form via ajax and receive report preview
	# formObj = $(event.target).closest("form")
	# $.ajax({
	# 	dataType: "script",
	# 	type: 'POST',
	# 	url: formObj.attr("action"),
	# 	data: formObj.serialize(),
	# });
	
	
	
	
# when updating the comment box, will update the report preview
$("#measurement_comment").bind "keydown", (event) =>
	$("#report-comment").html( $(event.target).val());
	
