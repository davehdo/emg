class NervesController < ApplicationController
  # GET /nerves
  # GET /nerves.json
  def index
    @nerves = Nerve.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @nerves }
    end
  end

  # GET /nerves/1
  # GET /nerves/1.json
  def show
    @nerve = Nerve.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @nerve }
    end
  end

  # GET /nerves/new
  # GET /nerves/new.json
  def new
    @nerve = Nerve.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @nerve }
    end
  end

  # GET /nerves/1/edit
  def edit
    @nerve = Nerve.find(params[:id])
  end

  # POST /nerves
  # POST /nerves.json
  def create
    @nerve = Nerve.new(params[:nerve])

    respond_to do |format|
      if @nerve.save
        format.html { redirect_to nerves_path, notice: 'Nerve was successfully created.' }
        format.json { render json: @nerve, status: :created, location: @nerve }
      else
        format.html { render action: "new" }
        format.json { render json: @nerve.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /nerves/1
  # PUT /nerves/1.json
  def update
    @nerve = Nerve.find(params[:id])

    respond_to do |format|
      if @nerve.update_attributes(params[:nerve])
        format.html { redirect_to @nerve, notice: 'Nerve was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @nerve.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nerves/1
  # DELETE /nerves/1.json
  def destroy
    @nerve = Nerve.find(params[:id])
    @nerve.destroy

    respond_to do |format|
      format.html { redirect_to nerves_url }
      format.json { head :no_content }
    end
  end
end
