class RecSessionsController < ApplicationController
  # GET /rec_sessions
  # GET /rec_sessions.json
  def index
    @rec_sessions = RecSession.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rec_sessions }
    end
  end

  # GET /rec_sessions/1
  # GET /rec_sessions/1.json
  def show
    @rec_session = RecSession.find(params[:id])
    @measurements = @rec_session.measurements.joins(:nerve).select("measurements.*, nerves.study_type as study_type,nerves.name as nerve_name")
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @rec_session }
    end
  end

  # GET /rec_sessions/new
  # GET /rec_sessions/new.json
  def new
    # @rec_session = RecSession.new
    
    if params[:patient_id]
      @patient = Patient.find(params[:patient_id])
      rec_session = @patient.rec_sessions.create!(:date=>Time.now)
      redirect_to rec_session
    else
      redirect_to root_path
    end
    
    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @rec_session }
    # end
  end

  # GET /rec_sessions/1/edit
  def edit
    @rec_session = RecSession.find(params[:id])
    3.times {@rec_session.session_complaints.build}
    
  end

  # POST /rec_sessions
  # POST /rec_sessions.json
  def create
    @rec_session = RecSession.new(params[:rec_session])

    respond_to do |format|
      if @rec_session.save
        format.html { redirect_to @rec_session, notice: 'Rec session was successfully created.' }
        format.json { render json: @rec_session, status: :created, location: @rec_session }
      else
        format.html { render action: "new" }
        format.json { render json: @rec_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /rec_sessions/1
  # PUT /rec_sessions/1.json
  def update
    @rec_session = RecSession.find(params[:id])

    respond_to do |format|
      if @rec_session.update_attributes(params[:rec_session])
        format.html { redirect_to @rec_session, notice: 'Rec session was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @rec_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rec_sessions/1
  # DELETE /rec_sessions/1.json
  def destroy
    @rec_session = RecSession.find(params[:id])
    @rec_session.destroy

    respond_to do |format|
      format.html { redirect_to rec_sessions_url }
      format.json { head :no_content }
    end
  end
end
