class SessionComplaint < ActiveRecord::Base
  belongs_to :rec_session
  belongs_to :complaint
  attr_accessible :body_part, :complaint_name, :session_complaints_attributes
  
  def self.body_parts
    [["LUE",1],["RUE", 2], ["BUE", 3], ["LLE", 4], ["RLE", 5], ["BLE", 6]]
  end
  
  def complaint_name
    self.complaint.name if self.complaint
  end
  
  def complaint_name=(name)
    if name.present?
      self.complaint = Complaint.reference_or_create_by_name(name)
    else
      self.complaint= nil
    end
  end
  
  def body_part_in_words
    s = SessionComplaint.body_parts.rassoc(self.body_part)
    s ? s[0] : "unlocalized" 
  end
  
  def one_liner
    if self.complaint_name.blank?
      nil
    else
      self.body_part_in_words + " " + self.complaint_name
    end
  end
end
