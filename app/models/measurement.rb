class Measurement < ActiveRecord::Base
  attr_accessible :side, :nerve_id, :rec_session_id, :amp_1_cat, :ncv_1_cat, :dist_1_cat, :lat_1_cat, :amp_1, :ncv_1, :dist_1, :lat_1, :amp_2_cat, :ncv_2_cat, :dist_2_cat, :lat_2_cat, :amp_2, :ncv_2, :dist_2, :lat_2, :amp_3_cat, :ncv_3_cat, :dist_3_cat, :lat_3_cat, :amp_3, :ncv_3, :dist_3, :lat_3, :comment
  # :amplitude_cat, :latency_cat, :location_id, :nerve_id, :side, :velocity_cat
  
  belongs_to :rec_session
  belongs_to :nerve
  
  def amp_units
    self.nerve.amp_units
  end
    
  def display_name
    "#{ self.side_abbrev || "" } #{self.nerve ? self.nerve.display_name : ""} #{"(#{self.one_liner})"}"
  end
  
  def lat_in_words( segment_number )
    a = Measurement.latency_choices.rassoc(self.send("lat_#{segment_number}_cat"))
    a ? a[0] : "UNDEFINED"
  end
  
  def amp_in_words( segment_number )
    a = Measurement.amplitude_choices.rassoc(self.send("amp_#{segment_number}_cat"))
    a ? a[0] : "UNDEFINED"    
  end
  
  def ncv_in_words( segment )
    a = Measurement.velocity_choices.rassoc(self.send("ncv_#{segment}_cat"))
    a ? a[0] : "UNDEFINED"
  end
  
  def segment_present?(n)
    [self.send("lat_#{n}_cat"), self.send("amp_#{n}_cat"), self.send("ncv_#{n}_cat")].any? {|e| !(e.nil? or e==0) }
  end
  
  def segments
    nerve = self.nerve
    [1,2,3].select {|e| self.segment_present?(e)}.collect {|n| {:stim_site => nerve ? nerve.stim_site_1 : nil, :amp => self.amp_in_words(n), :ncv => self.ncv_in_words(n), :dist => self.send("dist_#{n}"), :lat=>self.lat_in_words(n)}}
  end
  
  
  def report
    if (nerve=self.nerve)
      # [['Nerve conduction sensory', 1], ['Nerve conduction motor', 2], ["Comparison", 3], ["F response", 4], ["H reflex", 5]]
      case nerve.study_type
      
      when 1 # nerve conduction sensory
        if self.abnormal?
          "#{self.side_in_words} #{nerve.name} sensory potential to #{nerve.rec_site} showed #{self.segments.collect {|e| "#{e[:lat]} peak latency and #{e[:amp]} amplitude at the #{e[:stim_site]} segment" }.to_sentence}. #{self.comment.present? ? "<span id=\"report-comment\">#{self.comment}</span>" : ""}"
        elsif self.blank?
          ""
        else # present and normal          
          "#{self.side_in_words} #{nerve.name} sensory potential to #{nerve.rec_site} showed normal peak latency and normal amplitude."
        end
      when 2 # nerve conduction motor
          "#{self.side_in_words} #{nerve.name} motor potential to #{nerve.rec_site} showed #{self.segments.collect {|e| "#{ e[:lat]} onset latency, #{e[:amp]} amplitude, and #{e[:ncv]} conduction velocity"}.to_sentence}."
        # 3. Abbreviated Motor: {Test_Side list} {Motor_List} motor potential to {Motor_Location_List} showed { Latency_List} onset latency and {Amplitude_List} amplitude. 
        # 4. Abbreviated Motor: Bilateral {Motor_List} motor potential to {Motor_Location_List} showed { Latency_List} onset latencies and {Amplitude_List} amplitudes.
    
      when 3 # comparison studies
        "Comparison studies of #{self.side_in_words} {Sensory_Comparison_List} {Comparison_Location_List} showed a peak latency difference of {entry}ms."
      
      when 4
        "#{self.side_in_words} #{nerve.name} F-waves showed #{self.lat_in_words} latencies."
        # 2. Bilateral {Motor_List} F-waves showed {Latency_List} latencies.  
        
      when 5
        "#{self.side_in_words} #{nerve.name} H-waves showed #{self.lat_in_words} latencies."
          # 2. Bilateral {Motor_List} H-waves showed {Latency_List} latencies.  
      end
    else
      puts "Error in Measurement.report: this measurement does not belong to a nerve #{self.inspect}"
      ""
    end
  end
  
  def blank?
    ![self.lat_1_cat, self.lat_2_cat, self.lat_3_cat, self.amp_1_cat, self.amp_2_cat, self.amp_3_cat, self.ncv_1_cat, self.ncv_2_cat, self.ncv_3_cat].any? {|e| !(e.nil? or e==0) }
  end
  
  def abnormal?
    values = [self.lat_1_cat, self.lat_2_cat, self.lat_3_cat, self.amp_1_cat, self.amp_2_cat, self.amp_3_cat, self.ncv_1_cat, self.ncv_2_cat, self.ncv_3_cat]
    values.include?( 2) or values.include?( 3) or values.include?(4) or values.include?(5)
  end
  
  def one_liner
    if self.abnormal?  
      "Abnormal"
    elsif self.blank?
      "Blank"
    else
      "All normal"
    end
  end
  
  def side_in_words
    { 1 => "Left", 2 => "Right", 3 => "Bilateral"}[self.side] || "UNDEFINED SIDE"
    
  end
  
  def side_abbrev
    { nil => "", 1 => "L", 2 => "R", 3 => "B"}[self.side]
  end
  
  def side=(side_par)
      if side_par =~ /^l/i or [:left,1,"1"].include? side_par
        write_attribute(:side, 1)
      elsif side_par =~ /^r/i or [:right,2,"2"].include? side_par
        write_attribute(:side, 2)
        
      elsif side_par =~ /^b/i or [:bilateral,3,"3"].include? side_par
        write_attribute(:side, 3)
        
      end
      
    end
  
  def self.latency_choices
    [["not measured", 0], ["normal",1], ["borderline",2], ["prolonged",3], ["significantly prolonged",4], ["absent",5]]
  end
  
  def self.amplitude_choices
    [["not measured", 0], ["normal",1], ["reduced",2], ["significantly reduced",3], ["absent",4]]
  end
  
  def self.velocity_choices
    [["not measured", 0], ["normal",1], ["borderline",2], ["decreased",3], ["significantly decreased",4]]
  end
  
  def self.side_choices
    [['Left', 1], ['Right', 2], ["Bilateral",3]]
  end
  
  def self.nerve_sensory_choices
    [["Lateral Antebrachial", 1], ["Lateral Plantar", 2], ["Median", 3], ["Medial Antebrachial", 4], ["Medial Plantar", 5], ["Superficial Peroneal", 6], ["Radial", 7], ["Sural", 8], ["Ulnar",9]]
  end
  
  def self.nerve_motor_choices
    [["Accessory", 10], ["Axillary", 11], ["Facial", 12], ["Femoral", 13], ["Long Thoracic", 14], ["Median", 15], ["Musculocutaneous", 16], ["Peroneal", 17], ["Radial", 18], ["Tibial", 19], ["Ulnar", 20]]
  end
  
  def self.nerve_choices
    Measurement.nerve_sensory_choices.concat( Measurement.nerve_motor_choices)
  end
  
  def self.location_choices
    [['Lisbon', 1], ['Madrid', 2]]
  end
  
    
end
