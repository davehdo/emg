class Complaint < ActiveRecord::Base
  attr_accessible :count, :name
  has_many :session_complaints, :inverse_of =>:complaint
  has_many :rec_sessions, :through => :session_complaints
  def display_name
    "#{self.name} (#{self.session_complaints.size}/#{self.session_complaints.size})"
  end
  
  def self.reference_or_create_by_name( name )
    if name.blank?
      nil
    else
      a = where("lower(name) = ?", name.downcase)
      if a.size > 0
        a[0]
      else
        create(:name=>name)
      end
    end
  end
  
  def self.names
    select("name").collect{|e|e.name}
    
  end
  
  def self.names_with_quot
    select("name").collect{|e|"\"#{e.name}\""}
  end
end
