class Patient < ActiveRecord::Base
  
  include ActionView::Helpers::DateHelper
  
  attr_accessible :mrn, :name, :rec_sessions_attributes
  
  has_many :rec_sessions, :dependent=>:destroy
  
  accepts_nested_attributes_for :rec_sessions
  
  def display_name
    "#{self.name}#{ " (#{self.mrn})" if self.mrn.present? }" 
  end
  
  def name_and_mrn
    (self.name) + (self.mrn.present? ? " (#{self.mrn})" : "")
  end
  
  def name
    a = read_attribute(:name) 
    a.present? ? a : "Unnamed patient"
  end
  
  def updated_ago
    # if self.updated_at - Time.now < 7.days
      # self.updated_at.strftime("%Y-%m")
    # else
      time_ago_in_words(self.updated_at) + " ago"
    # end
  end
  
end
