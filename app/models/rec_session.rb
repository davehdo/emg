class RecSession < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  
  
  attr_accessible :date, :complaint_1, :session_complaints_attributes, :t_lue, :t_rue, :t_lle, :t_rle
  
  belongs_to :patient, :touch=>true #
  has_many :measurements
  has_many :session_complaints
  has_many :complaints, :through =>:session_complaints, :order=>"id ASC"
  # 
  accepts_nested_attributes_for :session_complaints, :reject_if => proc { |attributes| attributes['complaint_name'].blank? }
  
  def display_name
    "Session #{self.date} " +
  		(self.date.present? ?
  			"(#{ distance_of_time_in_words(self.date , Time.now.beginning_of_day).capitalize} ago)" : "")
  end
  
  def complaints_in_words
    if self.session_complaints.size > 0
      self.session_complaints.collect {|e|e.one_liner}.compact.to_sentence
    else
      "Unspecified complaint"
    end
  end
  
  def report
    self.measurements.joins(:nerve).order("nerves.study_type ASC").collect {|e| e.report}.join(" ")
  end
  
  def temp_in_words
    parts = {t_lue: "LUE", t_rue: "RUE", t_lle: "LLE", t_rle: "RLE"}.collect do |k,v|
      t = self.send(k)
      t ? "#{t} C in the #{v}" : nil
    end
    parts.compact.to_sentence
    
  end
end
