class Nerve < ActiveRecord::Base
  attr_accessible :name, :rec_site, :study_type, :ncv, :ncv_comment, :stim_site_1, :dist_1, :dist_comment_1, :lat_1, :lat_comment_1, :amp_1, :amp_comment_1, :ncv_1, :ncv_comment_1, :comment_1, :stim_site_2, :dist_2, :dist_comment_2, :lat_2, :lat_comment_2, :amp_2, :amp_comment_2, :ncv_2, :ncv_comment_2, :comment_2, :stim_site_3, :dist_3, :dist_comment_3, :lat_3, :lat_comment_3, :amp_3, :amp_comment_3, :ncv_3, :ncv_comment_3, :comment_3

  validates_presence_of :name
  
  has_many :measurements

  def amp_units
    if self.study_type==1
      "uV"
    elsif self.study_type==2
      "mV"
    else
      ""
    end
  end

  def self.study_types 
    [['Nerve conduction sensory', 1], ['Nerve conduction motor', 2], ["Comparison", 3], ["F response", 4], ["H reflex", 5]]
  end
  
  def study_type_in_words
    a = Nerve.study_types.rassoc( self.study_type )
    a ? a[0] : nil
  end
  
  def url_to_new_measurement_for_this_nerve( rec_session_id )
    "/rec_sessions/#{rec_session_id}/measurements/new?nerve_id=#{self.id}"
  end
  
  # ulnar nerve may have to different recording sites
  # so the specific name incoporates the recording site
  # e.g. ulnar (to digit 5) v ulnar (to FDI)
  
  def display_name
    self.specific_name
  end
  
  def specific_name
    (self.name || "Unnamed nerve") + (" (to #{self.rec_site})")
  end
  
  def name_is_unique_per_study_type?
    if self.name.present?
      Nerve.where("lower(name) = ?", self.name.downcase).where(:study_type=>self.study_type).size <= 1 #.exists?
    else
      false
    end
  end
  
end

