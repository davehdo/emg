require 'spec_helper'

describe "Measurements" do
  # describe "GET /measurements" do
  #   it "works! (now write some real specs)" do
  #     # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
  #     get measurements_path
  #     response.status.should be(200)
  #   end
  # end
  # 
  describe "new" do
    it "should load without fail" do
      @patient = Patient.create
      @session = @patient.rec_sessions.create
      get rec_session_path(@session) + "/measurements/new"
      response.status.should be(200)
    end

    
    it "should load without fail when provide nerve_id" do
      @patient = Patient.create
      @nerve = Nerve.create
      @session = @patient.rec_sessions.create
      get rec_session_path(@session) + "/measurements/new?nerve_id=#{@nerve.id}"
      response.status.should be(200)
    end
    
  end
  
  describe "edit" do
    it "should load without fail when provide nerve_id" do
      @patient = Patient.create
      @nerve = Nerve.create
      @session = @patient.rec_sessions.create
      @measurement = Measurement.create(:nerve_id=>@nerve.id , :rec_session_id=>@session.id)
      get rec_session_path(@session) + "/measurements/#{@measurement.id}/edit"
      response.status.should be(200)
    end
    
  end
  
end
