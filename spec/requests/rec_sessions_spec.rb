require 'spec_helper'

describe "RecSessions" do
  # describe "GET /rec_sessions" do
  #   it "works! (now write some real specs)" do
  #     # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
  #     get rec_sessions_path
  #     response.status.should be(200)
  #   end
  # end
  
  describe "GET /rec_session/1" do
    it "should render" do
      @patient = Patient.create
      @rec_session = @patient.rec_sessions.create
      get rec_session_path(@rec_session)
      response.status.should be(200)
    end
  end
  
end
