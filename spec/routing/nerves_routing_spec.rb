require "spec_helper"

describe NervesController do
  describe "routing" do

    it "routes to #index" do
      get("/nerves").should route_to("nerves#index")
    end

    it "routes to #new" do
      get("/nerves/new").should route_to("nerves#new")
    end

    it "routes to #show" do
      get("/nerves/1").should route_to("nerves#show", :id => "1")
    end

    it "routes to #edit" do
      get("/nerves/1/edit").should route_to("nerves#edit", :id => "1")
    end

    it "routes to #create" do
      post("/nerves").should route_to("nerves#create")
    end

    it "routes to #update" do
      put("/nerves/1").should route_to("nerves#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/nerves/1").should route_to("nerves#destroy", :id => "1")
    end

  end
end
