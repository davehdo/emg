# require 'spec_helper'
# 
# describe "nerves/index" do
#   before(:each) do
#     assign(:nerves, [
#       stub_model(Nerve,
#         :nerve => "Nerve",
#         :rec_site => "Rec Site",
#         :ncv => 1.5,
#         :stim_site_1 => "Stim Site 1",
#         :dist_1 => 1.5,
#         :lat_1 => 1.5,
#         :amp_1 => 1.5,
#         :ncv_1 => 1.5,
#         :comment_1 => "MyText"
#       ),
#       stub_model(Nerve,
#         :nerve => "Nerve",
#         :rec_site => "Rec Site",
#         :ncv => 1.5,
#         :stim_site_1 => "Stim Site 1",
#         :dist_1 => 1.5,
#         :lat_1 => 1.5,
#         :amp_1 => 1.5,
#         :ncv_1 => 1.5,
#         :comment_1 => "MyText"
#       )
#     ])
#   end
# 
#   it "renders a list of nerves" do
#     render
#     # Run the generator again with the --webrat flag if you want to use webrat matchers
#     assert_select "tr>td", :text => "Nerve".to_s, :count => 2
#     assert_select "tr>td", :text => "Rec Site".to_s, :count => 2
#     assert_select "tr>td", :text => 1.5.to_s, :count => 2
#     assert_select "tr>td", :text => "Stim Site 1".to_s, :count => 2
#     assert_select "tr>td", :text => 1.5.to_s, :count => 2
#     assert_select "tr>td", :text => 1.5.to_s, :count => 2
#     assert_select "tr>td", :text => 1.5.to_s, :count => 2
#     assert_select "tr>td", :text => 1.5.to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#   end
# end
