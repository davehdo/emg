# require 'spec_helper'
# 
# describe "nerves/new" do
#   before(:each) do
#     assign(:nerve, stub_model(Nerve,
#       :nerve => "MyString",
#       :rec_site => "MyString",
#       :ncv => 1.5,
#       :stim_site_1 => "MyString",
#       :dist_1 => 1.5,
#       :lat_1 => 1.5,
#       :amp_1 => 1.5,
#       :ncv_1 => 1.5,
#       :comment_1 => "MyText"
#     ).as_new_record)
#   end
# 
#   it "renders new nerve form" do
#     render
# 
#     # Run the generator again with the --webrat flag if you want to use webrat matchers
#     assert_select "form", :action => nerves_path, :method => "post" do
#       assert_select "input#nerve_nerve", :name => "nerve[nerve]"
#       assert_select "input#nerve_rec_site", :name => "nerve[rec_site]"
#       assert_select "input#nerve_ncv", :name => "nerve[ncv]"
#       assert_select "input#nerve_stim_site_1", :name => "nerve[stim_site_1]"
#       assert_select "input#nerve_dist_1", :name => "nerve[dist_1]"
#       assert_select "input#nerve_lat_1", :name => "nerve[lat_1]"
#       assert_select "input#nerve_amp_1", :name => "nerve[amp_1]"
#       assert_select "input#nerve_ncv_1", :name => "nerve[ncv_1]"
#       assert_select "textarea#nerve_comment_1", :name => "nerve[comment_1]"
#     end
#   end
# end
