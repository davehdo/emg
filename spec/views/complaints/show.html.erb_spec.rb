require 'spec_helper'

describe "complaints/show" do
  before(:each) do
    @complaint = assign(:complaint, stub_model(Complaint,
      :name => "Name",
      :count => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/1/)
  end
end
