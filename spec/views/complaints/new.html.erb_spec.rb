require 'spec_helper'

describe "complaints/new" do
  before(:each) do
    assign(:complaint, stub_model(Complaint,
      :name => "MyString",
      :count => 1
    ).as_new_record)
  end

  it "renders new complaint form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => complaints_path, :method => "post" do
      assert_select "input#complaint_name", :name => "complaint[name]"
      assert_select "input#complaint_count", :name => "complaint[count]"
    end
  end
end
