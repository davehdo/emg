require 'spec_helper'

describe "complaints/edit" do
  before(:each) do
    @complaint = assign(:complaint, stub_model(Complaint,
      :name => "MyString",
      :count => 1
    ))
  end

  it "renders the edit complaint form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => complaints_path(@complaint), :method => "post" do
      assert_select "input#complaint_name", :name => "complaint[name]"
      assert_select "input#complaint_count", :name => "complaint[count]"
    end
  end
end
