require 'spec_helper'

describe Nerve do

  describe "name_is_unique_per_study_type?" do
    it "should return true appropriately" do
      n = Nerve.create(:name=>"ulnar", :study_type=>1)
      n.name_is_unique_per_study_type?.should == true
    end
    
    it "should return false appropriately" do
      Nerve.create(:name=>"Ulnar", :study_type=>1)
      n = Nerve.create(:name=>"ulnar", :study_type=>1)
      n.name_is_unique_per_study_type?.should == false
    end
  end
  

end
