require 'spec_helper'

describe Complaint do
  describe "reference_or_create_by_name" do
    it "should return nil if name is blank" do
      Complaint.reference_or_create_by_name("").should == nil
    end
    
    it "should return nil if name is nil" do
      Complaint.reference_or_create_by_name(nil).should == nil
    end
    
  end
end
