require 'spec_helper'

describe Measurement do
  describe "side attribute" do
    it "should accept left as 1" do
      @m = Measurement.new(:side=>"left")
      @m.side.should == 1   
    end
    it "should accept LEFT as 1" do
      @m = Measurement.new(:side=>"LEFT")
      @m.side.should == 1   
    end

    it "should accept LEFT as 1" do
      @m = Measurement.new(:side=>"L")
      @m.side.should == 1   
    end

    it "should accept left as 1" do
      @m = Measurement.new(:side=>:left)
      @m.side.should == 1      
    end

    it "should accept 1 as 1" do
      @m = Measurement.new(:side=>1)
      @m.side.should == 1      
    end
  end
  
  describe "updating" do
    it "should be allowed by update_attributes with hash param" do
      @m = Measurement.create
      @m.update_attributes({:side=>"2"})
      @m.side.should == 2
    end
  end
  
  describe "blank? method" do
    it "should return true appropriately" do
      n = Measurement.create
      n.blank?.should==true
    end
    
    it "should return true appropriately 2" do
      n = Measurement.create(:lat_1_cat=>0)
      n.blank?.should==true      
    end
    
    it "should return false appropriately" do
      n = Measurement.create(:lat_1_cat=>1)
      n.blank?.should==false      
    end
  end
  
  describe "segment_present? method" do
    it "should return true and false appropriately" do
      n = Measurement.create(:lat_1_cat=>1)
      n.segment_present?(1).should == true
      n.segment_present?(2).should == false
      n.segment_present?(3).should == false
      
    end
  end
  
  describe "segments method" do
    it "should return only segments defined appropriately" do
      n = Measurement.create(:lat_1_cat=>1)
      n.segments.size.should == 1      
    end
    it "should return only 2 segments defined appropriately" do
      n = Measurement.create(:lat_1_cat=>1, :amp_2_cat=>1)
      n.segments.size.should == 2
    end
    
  end
  
  describe "report method" do
    it "should work" do
      n = Nerve.create(:study_type=>1, :stim_site_1=>"distal", :rec_site=>"mars", :name=>"ulnar")
      m = n.measurements.create(:lat_1_cat=>3, :side=>1)
      m.report.should == "Left ulnar sensory potential to mars showed prolonged peak latency and UNDEFINED amplitude at the distal segment. "
    end
  end
  
  describe "lat_in_words method" do
    it "should read lat_1_cat=3 as prolonged" do
      n = Nerve.create(:name=>"nerveses", :study_type=>1, :stim_site_1=>"ulnar", :rec_site=>"mars")
      m = n.measurements.create(:lat_1_cat=>3, :side=>1)
      m.lat_in_words(1).should == "prolonged"      
    end
  end
  
  describe "amp_in_words method" do
    it "should read amp_2_cat=3 as prolonged" do
      n = Nerve.create(:name=>"nerveses", :study_type=>1, :stim_site_1=>"ulnar", :rec_site=>"mars")
      m = n.measurements.create(:amp_2_cat=>3, :side=>1)
      m.amp_in_words(2).should == "significantly reduced"
      
    end
    
  end
  
end
