require 'spec_helper'

describe Patient do
  it "should update updated_on when a rec_session is created" do
    @patient = Patient.create
    time1 = @patient.updated_at
    sleep 1
    @patient.rec_sessions.create
    @patient.reload.updated_at.should_not == time1
  end
end
