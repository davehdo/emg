require 'spec_helper'

describe SessionComplaint do
  describe "complaint_name setter" do
    it "should create a complaint" do
      r = SessionComplaint.create(:complaint_name=>"chickenpox")
      r.complaint.name.should == "chickenpox"
    end
    
    it "should reference a complaint by name" do
      r = SessionComplaint.create(:complaint_name=>"chickenpox")
      r2 = SessionComplaint.create(:complaint_name=>"chickenpox")
      r.complaint.should == r2.complaint
    end
    
    it "should remove reference if set to blank" do
      r = SessionComplaint.create(:complaint_name=>"chickenpox")
      r.update_attribute(:complaint_name, "")
      r.reload.complaint_id.should==nil
    end
    
    it "should create another complaint if name changed" do
      r = SessionComplaint.create(:complaint_name=>"chickenpox")
      r.update_attribute(:complaint_name, "not chickenpox")
      r.complaint.name.should == "not chickenpox"      
    end
    
    it "should reference with case insensitivity" do
      r = SessionComplaint.create(:complaint_name=>"chickenpox")
      r2 = SessionComplaint.create(:complaint_name=>"CHICKENPOX")
      r.complaint.should == r2.complaint
      
    end
    
    it "should ignore blank" do
      r = SessionComplaint.create(:complaint_name=>"")
      r.complaint.should == nil
      
    end
    
  end
end
