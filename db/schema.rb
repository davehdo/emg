# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130122022150) do

  create_table "complaints", :force => true do |t|
    t.string   "name"
    t.integer  "count"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "measurements", :force => true do |t|
    t.integer  "side"
    t.integer  "nerve_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "rec_session_id"
    t.integer  "amp_1_cat"
    t.integer  "ncv_1_cat"
    t.integer  "dist_1_cat"
    t.integer  "lat_1_cat"
    t.float    "amp_1"
    t.float    "ncv_1"
    t.float    "dist_1"
    t.float    "lat_1"
    t.integer  "amp_2_cat"
    t.integer  "ncv_2_cat"
    t.integer  "dist_2_cat"
    t.integer  "lat_2_cat"
    t.float    "amp_2"
    t.float    "ncv_2"
    t.float    "dist_2"
    t.float    "lat_2"
    t.integer  "amp_3_cat"
    t.integer  "ncv_3_cat"
    t.integer  "dist_3_cat"
    t.integer  "lat_3_cat"
    t.float    "amp_3"
    t.float    "ncv_3"
    t.float    "dist_3"
    t.float    "lat_3"
    t.text     "comment"
  end

  create_table "nerves", :force => true do |t|
    t.string   "name"
    t.string   "rec_site"
    t.integer  "study_type"
    t.float    "ncv"
    t.string   "ncv_comment"
    t.string   "stim_site_1"
    t.float    "dist_1"
    t.string   "dist_comment_1"
    t.float    "lat_1"
    t.string   "lat_comment_1"
    t.float    "amp_1"
    t.string   "amp_comment_1"
    t.float    "ncv_1"
    t.string   "ncv_comment_1"
    t.text     "comment_1"
    t.string   "stim_site_2"
    t.float    "dist_2"
    t.string   "dist_comment_2"
    t.float    "lat_2"
    t.string   "lat_comment_2"
    t.float    "amp_2"
    t.string   "amp_comment_2"
    t.float    "ncv_2"
    t.string   "ncv_comment_2"
    t.text     "comment_2"
    t.string   "stim_site_3"
    t.float    "dist_3"
    t.string   "dist_comment_3"
    t.float    "lat_3"
    t.string   "lat_comment_3"
    t.float    "amp_3"
    t.string   "amp_comment_3"
    t.float    "ncv_3"
    t.string   "ncv_comment_3"
    t.text     "comment_3"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "patients", :force => true do |t|
    t.string   "name"
    t.string   "mrn"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rec_sessions", :force => true do |t|
    t.date     "date"
    t.integer  "patient_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.float    "t_lue"
    t.float    "t_rue"
    t.float    "t_lle"
    t.float    "t_rle"
  end

  create_table "session_complaints", :force => true do |t|
    t.integer  "rec_session_id"
    t.integer  "complaint_id"
    t.integer  "body_part"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "session_complaints", ["complaint_id"], :name => "index_session_complaints_on_complaint_id"
  add_index "session_complaints", ["rec_session_id"], :name => "index_session_complaints_on_rec_session_id"

end
