class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.string :name
      t.integer :count

      t.timestamps
    end
  end
end
