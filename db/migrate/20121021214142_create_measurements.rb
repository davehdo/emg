class CreateMeasurements < ActiveRecord::Migration
  def change
    create_table :measurements do |t|
      t.integer :side
      t.integer :nerve_id
      t.integer :location_id
      t.integer :latency_cat
      t.integer :amplitude_cat
      t.integer :velocity_cat

      t.timestamps
    end
  end
end
