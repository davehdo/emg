class AddRecSessionIdToMeasurement < ActiveRecord::Migration
  def change
    add_column :measurements, :rec_session_id, :integer
  end
end
