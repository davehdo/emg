class CreateSessionComplaints < ActiveRecord::Migration
  def change
    create_table :session_complaints do |t|
      t.references :rec_session
      t.references :complaint
      t.integer :body_part

      t.timestamps
    end
    add_index :session_complaints, :rec_session_id
    add_index :session_complaints, :complaint_id
  end
end
