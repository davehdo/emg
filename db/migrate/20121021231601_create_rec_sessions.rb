class CreateRecSessions < ActiveRecord::Migration
  def change
    create_table :rec_sessions do |t|
      t.date :date
      t.integer :patient_id
      t.timestamps
    end
  end
end
