class AddTempsToRecSession < ActiveRecord::Migration
  def change
    add_column :rec_sessions, :t_lue, :float
    add_column :rec_sessions, :t_rue, :float
    add_column :rec_sessions, :t_lle, :float
    add_column :rec_sessions, :t_rle, :float
  end
end
