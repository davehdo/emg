class AddFieldsToMeasurement < ActiveRecord::Migration
  def change
    remove_column :measurements, :location_id
    remove_column :measurements, :latency_cat
    remove_column :measurements, :amplitude_cat
    remove_column :measurements, :velocity_cat

    add_column :measurements, :amp_1_cat, :integer
    add_column :measurements, :ncv_1_cat, :integer
    add_column :measurements, :dist_1_cat, :integer
    add_column :measurements, :lat_1_cat, :integer
    add_column :measurements, :amp_1, :float
    add_column :measurements, :ncv_1, :float
    add_column :measurements, :dist_1, :float
    add_column :measurements, :lat_1, :float
    
    add_column :measurements, :amp_2_cat, :integer
    add_column :measurements, :ncv_2_cat, :integer
    add_column :measurements, :dist_2_cat, :integer
    add_column :measurements, :lat_2_cat, :integer
    add_column :measurements, :amp_2, :float
    add_column :measurements, :ncv_2, :float
    add_column :measurements, :dist_2, :float
    add_column :measurements, :lat_2, :float
    
    add_column :measurements, :amp_3_cat, :integer
    add_column :measurements, :ncv_3_cat, :integer
    add_column :measurements, :dist_3_cat, :integer
    add_column :measurements, :lat_3_cat, :integer
    add_column :measurements, :amp_3, :float
    add_column :measurements, :ncv_3, :float
    add_column :measurements, :dist_3, :float
    add_column :measurements, :lat_3, :float
    
    add_column :measurements, :comment, :text
  end
end
