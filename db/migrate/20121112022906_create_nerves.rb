class CreateNerves < ActiveRecord::Migration
  def change
    create_table :nerves do |t|
      t.string :name
      t.string :rec_site
      t.integer :study_type
      
      t.float :ncv
      t.string :ncv_comment
      
      t.string :stim_site_1
      t.float :dist_1
      t.string :dist_comment_1
      t.float :lat_1
      t.string :lat_comment_1
      t.float :amp_1
      t.string :amp_comment_1
      t.float :ncv_1
      t.string :ncv_comment_1
      t.text :comment_1

      t.string :stim_site_2
      t.float :dist_2
      t.string :dist_comment_2
      t.float :lat_2
      t.string :lat_comment_2
      t.float :amp_2
      t.string :amp_comment_2
      t.float :ncv_2
      t.string :ncv_comment_2
      t.text :comment_2

      t.string :stim_site_3
      t.float :dist_3
      t.string :dist_comment_3
      t.float :lat_3
      t.string :lat_comment_3
      t.float :amp_3
      t.string :amp_comment_3
      t.float :ncv_3
      t.string :ncv_comment_3
      t.text :comment_3

      t.timestamps
    end
  end
end
